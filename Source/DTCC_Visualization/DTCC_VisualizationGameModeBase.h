// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DTCC_VisualizationGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DTCC_VISUALIZATION_API ADTCC_VisualizationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
